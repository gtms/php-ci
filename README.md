PHP CI Docker images
==================

This repo contains Docker image for PHP 7.1, 7.2, 7.3 and 7.4 which are based on the official PHP Docker images.

Images contain the following additions to the vanilla PHP images:

* Version control packages (git)
* git-ftp
* composer
* node.js and npm
* PHP extensions:
    * curl
    * gd
    * gettext
    * intl
    * json
    * mcrypt
    * mbstring
    * opcache
    * pdo_mysql
    * soap
    * xsl
    * zip
    * xdebug

Usage
------------------

``dgtms/php-ci:7.1``

``dgtms/php-ci:7.2``

``dgtms/php-ci:7.3``

``dgtms/php-ci:7.4``
